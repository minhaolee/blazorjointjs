# BlazorJointJS

Blazor component for clientIO / joint.

I am using Vs2022 and net 6 SDK.

This Blazor WASM project uses the JointJS library which could be repaid to develop Web visualization and interacts with diagrams and graphs.

JointJS is a JavaScript diagramming library. It can be used to create either static diagrams or, and more
importantly, fully interactive diagramming tools and application builders.

Please see [http://jointjs.com](http://jointjs.com) for more information, demos and documentation.

Or check out JointJS  [mind-map documentation](https://resources.jointjs.com/mmap/joint.html).



## Implementation JointJS with Blazor   

This defined as a meta model. we will Implementing all essential models in DTDL was straightforward given the capabilities of DTDL. And it demo with JointJS FBD sample.

![](./res/jointjsfbdsample.jpg)





## Getting started

#### To make it easy for you to get started with JointJs.

- #### JointJS tutorial 

  https://resources.jointjs.com/tutorial

- #### JointJS demos

  https://resources.jointjs.com/demos/chatbot

- #### JointJS做一個簡單的功能控製圖

  http://www.bjhee.com/jointjs.html

- #### JointJS中文文檔

  https://blog.csdn.net/weixin_34168880/article/details/88678509







## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/minhaolee/blazorjointjs.git
git branch -M main
git push -uf origin main
```



## Contributing
Minhao lee

## Authors and acknowledgment
mhlee0328@hotmail.com

## License
MIT
